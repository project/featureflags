<?php

declare(strict_types=1);

namespace Drupal\Tests\featureflags\Kernel;

use Drupal\featureflags\Entity\FeatureFlag;
use Drupal\featureflags\Plugin\Condition\FeatureFlagStatus;
use Drupal\KernelTests\KernelTestBase;

/**
 * Defines a class for testing feature flags.
 *
 * @group featureflags
 */
class FeatureFlagKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'featureflags',
    'system',
  ];

  /**
   * Tests the config entity.
   */
  public function testConfigEntity(): void {
    $id = $this->randomMachineName();
    $this->assertFalse(FeatureFlag::isActive($id));
    $this->assertNull(FeatureFlag::setActive($id));
    $this->assertNull(FeatureFlag::setInactive($id));

    $flag = FeatureFlag::create([
      'id' => $id,
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
    ]);
    $flag->save();

    $this->assertFalse(FeatureFlag::isActive($id));

    FeatureFlag::setActive($id);
    $this->assertTrue($flag->getState());

    FeatureFlag::setInactive($id);
    $this->assertFalse($flag->getState());

    $flag->setState(TRUE);
    $this->assertTrue($flag->getState());
    $context = \Drupal::service('cache_context.featureflags');
    $this->assertEquals('1', $context->getContext($id));
    $this->assertContains('config:feature_flag.flag.' . $id, $context->getCacheableMetadata($id)->getCacheTags());

    $flag->setState(FALSE);
    $this->assertFalse($flag->getState());
    $this->assertEquals('0', $context->getContext($id));

    $this->assertTrue(\Drupal::keyValue('featureflags')->has($id));
    $flag->delete();
    $this->assertFalse(\Drupal::keyValue('featureflags')->has($id));
  }

  /**
   * Tests condition plugin.
   */
  public function testCondition(): void {
    $id1 = $this->randomMachineName();
    $id2 = $this->randomMachineName();

    $flag1 = FeatureFlag::create([
      'id' => $id1,
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
    ]);
    $flag1->save();

    $flag2 = FeatureFlag::create([
      'id' => $id2,
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
    ]);
    $flag2->save();

    $condition = $this->container->get('plugin.manager.condition')->createInstance('featureflags', [
      'flags' => [$flag1->id()],
      'conjunction' => FeatureFlagStatus::CONJUNCTION_AND,
    ]);
    $this->assertFalse($condition->evaluate());

    $flag1->setState(TRUE);
    $this->assertTrue($condition->evaluate());

    $condition = $this->container->get('plugin.manager.condition')->createInstance('featureflags', [
      'flags' => [$flag1->id(), $flag2->id()],
      'conjunction' => FeatureFlagStatus::CONJUNCTION_AND,
    ]);

    $this->assertFalse($condition->evaluate());

    $flag2->setState(TRUE);
    $this->assertTrue($condition->evaluate());

    $flag1->setState(FALSE);
    $flag2->setState(FALSE);

    $condition = $this->container->get('plugin.manager.condition')->createInstance('featureflags', [
      'flags' => [$flag1->id(), $flag2->id()],
      'conjunction' => FeatureFlagStatus::CONJUNCTION_OR,
    ]);
    $this->assertFalse($condition->evaluate());

    $flag2->setState(TRUE);
    $this->assertTrue($condition->evaluate());

  }

}
