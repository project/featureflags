<?php

declare(strict_types=1);

namespace Drupal\Tests\featureflags\Functional;

use Drupal\Core\Url;
use Drupal\featureflags\Entity\FeatureFlag;
use Drupal\featureflags\Entity\FeatureFlagInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Defines a class for testing feature flags.
 *
 * @group featureflags
 */
class FeatureFlagAdminTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['featureflags', 'block', 'system'];

  /**
   * User interface.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->createUser([
      'access administration pages',
      'administer featureflag entities',
    ]);
    $this->drupalPlaceBlock('local_actions_block');
  }

  /**
   * Tests flag administration.
   */
  public function testFeatureFlagAdmin() {
    $this->checkThatAnonymousUserCannotAdministerFeatureFlags();
    $flag = $this->checkThatAdminCanAddFeatureFlags();
    $flag = $this->checkThatAdminCanEditFeatureFlags($flag);
    $this->checkThatAdminCanDeleteFeatureFlags($flag);
  }

  /**
   * Tests anonymous users can't access admin routes.
   */
  private function checkThatAnonymousUserCannotAdministerFeatureFlags() : void {
    $flag = FeatureFlag::create([
      'id' => $this->randomMachineName(),
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
    ]);
    $flag->save();
    $urls = [
      Url::fromRoute('entity.featureflag.collection'),
      $flag->toUrl('edit-form'),
      $flag->toUrl('delete-form'),
    ];
    foreach ($urls as $url) {
      $this->drupalGet($url);
      $this->assertSession()->statusCodeEquals(403);
    }
  }

  /**
   * Assert that admin can add a flag.
   *
   * @return \Drupal\featureflags\Entity\FeatureFlagInterface
   *   The added flag.
   */
  private function checkThatAdminCanAddFeatureFlags() : FeatureFlagInterface {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet(Url::fromRoute('system.admin_structure'));
    $assert = $this->assertSession();
    $assert->linkExists('Feature flags');
    $this->drupalGet(Url::fromRoute('entity.featureflag.collection'));
    $assert->statusCodeEquals(200);
    $assert->linkExists('Add feature flag');
    $this->clickLink('Add feature flag');
    $this->assertStringContainsString(Url::fromRoute('entity.featureflag.add_form')->toString(), $this->getSession()->getCurrentUrl());
    $name = $this->randomMachineName();
    $id = mb_strtolower($this->randomMachineName());
    $this->submitForm([
      'id' => $id,
      'name' => $name,
      'description' => $this->randomString(),
    ], 'Save');
    $assert->pageTextContains(sprintf('Created the %s feature flag.', $name));
    $assert->linkExists($name);
    return FeatureFlag::load($id);
  }

  /**
   * Assert that admin can edit flags.
   *
   * @param \Drupal\featureflags\Entity\FeatureFlagInterface $flag
   *   Flag to edit.
   *
   * @return \Drupal\featureflags\Entity\FeatureFlagInterface
   *   The edited flag.
   */
  private function checkThatAdminCanEditFeatureFlags(FeatureFlagInterface $flag) : FeatureFlagInterface {
    $this->drupalGet(Url::fromRoute('entity.featureflag.collection'));
    $assert = $this->assertSession();
    $edit = $flag->toUrl('edit-form');
    $assert->linkByHrefExists($edit->toString());
    $this->drupalGet($edit);
    $assert->fieldValueEquals('name', $flag->label());
    $assert->fieldValueEquals('description', $flag->getDescription());
    $new_name = $this->randomMachineName();
    $this->submitForm([
      'name' => $new_name,
      'active' => TRUE,
    ], 'Save');
    $assert->pageTextContains(sprintf('Saved the %s feature flag.', $new_name));
    $this->assertTrue($flag->getState());
    $this->drupalGet($edit);
    $this->submitForm([
      'active' => FALSE,
    ], 'Save');
    \Drupal::service('featureflags.manager')->resetCache();
    $this->assertFalse($flag->getState());
    return \Drupal::entityTypeManager()->getStorage('featureflag')->loadUnchanged($flag->id());
  }

  /**
   * Assert that admin can delete flags.
   *
   * @param \Drupal\featureflags\Entity\FeatureFlagInterface $flag
   *   The flag to delete.
   */
  private function checkThatAdminCanDeleteFeatureFlags(FeatureFlagInterface $flag) : void {
    $this->drupalGet(Url::fromRoute('entity.featureflag.collection'));
    $assert = $this->assertSession();
    $delete = $flag->toUrl('delete-form');
    $assert->linkByHrefExists($delete->toString());
    $this->drupalGet($delete);
    $this->submitForm([], 'Delete');
    $assert->pageTextContains(sprintf('The feature flag %s has been deleted.', $flag->label()));
  }

}
