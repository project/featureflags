<?php

declare(strict_types=1);

namespace Drupal\featureflags\EntityHandlers;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\featureflags\Entity\FeatureFlagInterface;

/**
 * Defines a list builder for feature flags.
 */
class FeatureFlagListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'name' => $this->t('Name'),
      'description' => $this->t('Description'),
      'status' => $this->t('Status'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    assert($entity instanceof FeatureFlagInterface);
    return [
      'name' => $entity->toLink($entity->label(), 'edit-form'),
      'description' => $entity->getDescription(),
      'status' => $entity->getState() ? $this->t('Active') : $this->t('Inactive'),
    ] + parent::buildRow($entity);
  }

}
