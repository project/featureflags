<?php

declare(strict_types=1);

namespace Drupal\featureflags\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines a config entity for a feature flag.
 *
 * @ConfigEntityType(
 *   id = "featureflag",
 *   label = @Translation("Feature flag"),
 *   label_singular = @Translation("feature flag"),
 *   label_plural = @Translation("feature flags"),
 *   label_collection = @Translation("Feature flags"),
 *   label_count = @PluralTranslation(
 *     singular = "@count feature flag",
 *     plural = "@count feature flags"
 *   ),
 *   handlers = {
 *     "list_builder" = \Drupal\featureflags\EntityHandlers\FeatureFlagListBuilder::class,
 *     "access" = \Drupal\Core\Entity\EntityAccessControlHandler::class,
 *     "form" = {
 *       "default" = \Drupal\featureflags\Form\FeatureFlagForm::class,
 *       "delete" = \Drupal\Core\Entity\EntityDeleteForm::class,
 *     },
 *     "route_provider" = {
 *       "html" = \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider::class,
 *     }
 *   },
 *   admin_permission = "administer featureflag entities",
 *   config_prefix = "flag",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/feature-flags/add",
 *     "delete-form" = "/admin/structure/feature-flags/manage/{featureflag}/delete",
 *     "edit-form" = "/admin/structure/feature-flags/manage/{featureflag}",
 *     "collection" = "/admin/structure/feature-flags",
 *   },
 *   config_export = {
 *     "name",
 *     "id",
 *     "description",
 *   }
 * )
 */
class FeatureFlag extends ConfigEntityBase implements FeatureFlagInterface {

  /**
   * ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Name.
   *
   * @var string
   */
  protected $name;

  /**
   * Description.
   *
   * @var string
   */
  protected $description;

  /**
   * Sets this feature flag as active.
   */
  protected function activate(): void {
    \Drupal::service('featureflags.manager')->set($this->id(), TRUE);
    Cache::invalidateTags($this->getCacheTagsToInvalidate());
  }

  /**
   * Sets this feature flag as inactive.
   */
  protected function inactivate(): void {
    \Drupal::service('featureflags.manager')->set($this->id(), FALSE);
    Cache::invalidateTags($this->getCacheTagsToInvalidate());
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): bool {
    return \Drupal::service('featureflags.manager')->get($this->id(), FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function setState(bool $state): FeatureFlagInterface {
    \Drupal::service('featureflags.manager')->set($this->id(), $state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    \Drupal::service('featureflags.manager')->deleteMultiple(array_map(function (FeatureFlagInterface $flag) {
      return $flag->id();
    }, $entities));
  }

  /**
   * {@inheritdoc}
   */
  public static function setActive(string $flag_id): ?FeatureFlagInterface {
    if (!($flag = self::load($flag_id))) {
      return NULL;
    }
    $flag->activate();
    return $flag;
  }

  /**
   * {@inheritdoc}
   */
  public static function setInactive(string $flag_id): ?FeatureFlagInterface {
    if (!($flag = self::load($flag_id))) {
      return NULL;
    }
    $flag->inactivate();
    return $flag;
  }

  /**
   * {@inheritdoc}
   */
  public static function isActive(string $id): bool {
    if (!($flag = self::load($id))) {
      return FALSE;
    }
    return $flag->getState();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->description;
  }

}
