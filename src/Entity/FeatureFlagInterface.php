<?php

declare(strict_types=1);

namespace Drupal\featureflags\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines an interface for a feature flag.
 */
interface FeatureFlagInterface extends ConfigEntityInterface {

  /**
   * Check if the given flag is active.
   *
   * @param string $id
   *   Flag ID.
   *
   * @return bool
   *   TRUE if the flag is active.
   */
  public static function isActive(string $id): bool;

  /**
   * Sets the flag as active.
   *
   * @param string $flag_id
   *   Flag ID.
   *
   * @return \Drupal\featureflags\Entity\FeatureFlagInterface|null
   *   The flag if it exists, otherwise NULL.
   */
  public static function setActive(string $flag_id): ?FeatureFlagInterface;

  /**
   * Sets the flag as inactive.
   *
   * @param string $flag_id
   *   Flag ID.
   *
   * @return \Drupal\featureflags\Entity\FeatureFlagInterface|null
   *   The flag if it exists, otherwise NULL.
   */
  public static function setInactive(string $flag_id): ?FeatureFlagInterface;

  /**
   * Gets the flag description if its set.
   *
   * @return string|null
   *   Flag description.
   */
  public function getDescription(): ?string;

  /**
   * Gets the current state of the flag.
   */
  public function getState(): bool;

  /**
   * Sets the current state.
   *
   * @param bool $state
   *   State.
   *
   * @return \Drupal\featureflags\Entity\FeatureFlagInterface
   *   Feature flag.
   */
  public function setState(bool $state): FeatureFlagInterface;

}
