<?php

declare(strict_types=1);

namespace Drupal\featureflags\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\featureflags\Entity\FeatureFlagInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a condition plugin for feature flags.
 *
 * @Condition(
 *   id = "featureflags",
 *   label = @Translation("Feature flags"),
 * )
 */
class FeatureFlagStatus extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  const CONJUNCTION_AND = 'AND';
  const CONJUNCTION_OR = 'OR';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs a new FeatureFlagStatus.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param array $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'flags' => [],
      'conjunction' => static::CONJUNCTION_OR,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $tags = [];
    foreach ($this->entityTypeManager->getStorage('featureflag')->loadMultiple($this->configuration['flags']) as $flag) {
      $tags = array_merge($tags, $flag->getCacheTagsToInvalidate());
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['flags'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getFeatureFlagOptions(),
      '#title' => $this->t('Feature flags'),
      '#description' => $this->t('Select relevant feature flags.'),
      '#default_value' => $this->configuration['flags'],
    ];
    $form['conjunction'] = [
      '#default_value' => $this->configuration['conjunction'],
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => [
        static::CONJUNCTION_OR => $this->t('OR'),
        static::CONJUNCTION_AND => $this->t('AND'),
      ],
      '#title' => $this->t('Conjunction'),
      static::CONJUNCTION_OR => [
        '#description' => $this->t('At least one selected feature flag is active'),
      ],
      static::CONJUNCTION_AND => [
        '#description' => $this->t('All selected feature flags are active'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $enabled_flags = array_filter(array_values($form_state->getValue('flags')));
    if (empty($enabled_flags)) {
      // Cleanup so the condition plugin isn't seen as active.
      unset($this->configuration['flags'], $this->configuration['conjunction']);
      return;
    }
    $this->configuration['flags'] = $enabled_flags;
    $this->configuration['conjunction'] = $form_state->getValue('conjunction');
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $conjunction = $this->configuration['conjunction'];
    foreach ($this->entityTypeManager->getStorage('featureflag')->loadMultiple($this->configuration['flags']) as $flag) {
      assert($flag instanceof FeatureFlagInterface);
      $state = $flag->getState();
      if ($conjunction === static::CONJUNCTION_OR && $state) {
        return TRUE;
      }
      if ($conjunction === static::CONJUNCTION_AND && !$state) {
        return FALSE;
      }
    }
    return $conjunction === static::CONJUNCTION_AND;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $enabled_options = array_intersect_key($this->getFeatureFlagOptions(), array_flip($this->configuration['flags']));
    $flag_string = implode(', ', $enabled_options);
    if (count($enabled_options) > 1) {
      $last = array_pop($enabled_options);
      $flag_string = $this->t('@rest @conjunction @last', [
        '@rest' => implode(', ', $enabled_options),
        '@conjunction' => mb_strtolower($this->configuration['conjunction']),
        '@last' => $last,
      ]);
    }
    return $this->t('Returns @state if @conjunction of the flags @flags are active', [
      '@state' => $this->isNegated() ? $this->t('true') : $this->t('false'),
      '@conjunction' => $this->configuration['conjunction'] === static::CONJUNCTION_AND ? $this->t('all') : $this->t('any'),
      '@flags' => $flag_string,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    if (!$this->configuration['flags']) {
      return $dependencies;
    }
    foreach ($this->entityTypeManager->getStorage('featureflag')->loadMultiple($this->configuration['flags']) as $flag) {
      assert($flag instanceof FeatureFlagInterface);
      $dependencies[$flag->getConfigDependencyKey()][] = $flag->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * Gets feature flag options.
   *
   * @return array
   *   Array of flags keyed by ID.
   */
  private function getFeatureFlagOptions(): array {
    return array_map(function (FeatureFlagInterface $flag) {
      return $flag->label();
    }, $this->entityTypeManager->getStorage('featureflag')->loadMultiple());
  }

}
