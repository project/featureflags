<?php

declare(strict_types=1);

namespace Drupal\featureflags;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a class for a feature flags cache context.
 *
 * Use as follows:
 *   - featureflags:{id} where id is the feature flag ID,
 *     e.g featureflags:new_site.
 */
class FeatureFlagContext implements CacheContextInterface {

  /**
   * Feature flag manager.
   *
   * @var \Drupal\featureflags\FlagManager
   */
  protected $flagManager;

  /**
   * Constructs a new FeatureFlagContext.
   *
   * @param \Drupal\featureflags\FlagManager $flagManager
   *   Feature flag manager.
   */
  public function __construct(FlagManager $flagManager) {
    $this->flagManager = $flagManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return new TranslatableMarkup('Feature flags');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(string $flag_id = NULL): string {
    return $this->flagManager->get($flag_id, FALSE) ? '1' : '0';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata(string $flag_id = NULL): CacheableMetadata {
    return (new CacheableMetadata())->addCacheTags(['config:feature_flag.flag.' . $flag_id]);
  }

}
