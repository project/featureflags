<?php

declare(strict_types=1);

namespace Drupal\featureflags\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\featureflags\Entity\FeatureFlagInterface;

/**
 * Defines a form for editing feature flags.
 */
class FeatureFlagForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $flag = $this->entity;
    assert($flag instanceof FeatureFlagInterface);
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $flag->label(),
      '#description' => $this->t("Name for the feature flag."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $flag->id(),
      '#maxlength' => EntityTypeInterface::ID_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\featureflags\Entity\FeatureFlag::load',
        'source' => ['name'],
      ],
      '#disabled' => !$flag->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $flag->getDescription(),
      '#description' => $this->t("Description for the feature flag."),
    ];

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $flag->getState(),
      '#description' => $this->t('Whether or not this feature flag is active'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $flag = $this->entity;
    assert($flag instanceof FeatureFlagInterface);
    $status = $flag->save();
    $flag->setState((bool) $form_state->getValue('active'));

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label feature flag.', [
          '%label' => $flag->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label feature flag.', [
          '%label' => $flag->label(),
        ]));
    }
    $form_state->setRedirectUrl($flag->toUrl('collection'));
  }

}
