<?php

declare(strict_types=1);

namespace Drupal\featureflags;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\State;

/**
 * Defines a class for managing feature flags.
 */
class FlagManager extends State {

  /**
   * {@inheritdoc}
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory, CacheBackendInterface $cache, LockBackendInterface $lock) {
    parent::__construct($key_value_factory, $cache, $lock);
    $this->keyValueStore = $key_value_factory->get('featureflags');
  }

  /**
   * {@inheritdoc}
   */
  public function get($key, $default = NULL) {
    // Prime the lot in a single query.
    if (empty($this->cache)) {
      $this->cache = $this->keyValueStore->getAll();
    }
    return parent::get($key, $default);
  }

}
